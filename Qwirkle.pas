program Qwirkle;


uses UMain, sysutils;

//Permet de mettre la valeur à la variable correspondante en fonction de la position de l'argument.
Procedure getArg(i : Integer; Var color, formes, nbtuiles : Integer; Var joueur : String);
	Begin
	If ParamStr(i) = '-j' Then
		joueur := ParamStr(i+1)
	Else If ParamStr(i) = '-c' Then
		color := StrToInt(ParamStr(i+1))
	Else If ParamStr(i) = '-f' Then
		formes := StrToInt(ParamStr(i+1))
	Else If ParamStr(i) = '-t' Then
		nbtuiles := StrToInt(ParamStr(i+1))
	End;

//Paramètre du jeu
var color, formes, nbtuiles : Integer;
	joueur : String;
begin
	//Valeur par défaut
	joueur := 'hh';
	color := 6;
	formes := 6;
	nbtuiles := 3;
	getArg(1, color, formes, nbtuiles, joueur);
	getArg(3, color, formes, nbtuiles, joueur);
	getArg(5, color, formes, nbtuiles, joueur);
	getArg(7, color, formes, nbtuiles, joueur);
	
	UMain.create(joueur, formes, color, nbtuiles); //Appel de la fonction pour créer le jeu et le lance
end.
