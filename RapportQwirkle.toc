\select@language {french}
\contentsline {paragraph}{Introduction}{2}{section*.2}
\contentsline {section}{\numberline {I}Organisation générale}{2}{section.0.1}
\contentsline {subsection}{\numberline {I.1}Une organisation en Units}{2}{subsection.0.1.1}
\contentsline {subsection}{\numberline {I.2}Types et variables}{2}{subsection.0.1.2}
\contentsline {section}{\numberline {II}Les Units}{5}{section.0.2}
\contentsline {subsection}{\numberline {II.1}Qwirkle}{5}{subsection.0.2.1}
\contentsline {subsection}{\numberline {II.2}UMain}{5}{subsection.0.2.2}
\contentsline {subsection}{\numberline {II.3}Utils}{5}{subsection.0.2.3}
\contentsline {subsection}{\numberline {II.4}UPlayer}{5}{subsection.0.2.4}
\contentsline {subsection}{\numberline {II.5}UHuman}{5}{subsection.0.2.5}
\contentsline {subsection}{\numberline {II.6}UComputer}{5}{subsection.0.2.6}
\contentsline {subsection}{\numberline {II.7}UMap}{5}{subsection.0.2.7}
\contentsline {section}{\numberline {III}L'interface graphique}{6}{section.0.3}
