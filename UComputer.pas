{$mode objfpc}
{$m+}
unit UComputer;
interface
	uses Utils, UPlayer, chaine, UMap, Glib2d, sysutils, Crt;

	type Computer = 	class(Player)
		public
			constructor create(nom : String);
		function play() : Boolean; override;
		function testPoint( pos : posCartes) : mem;
		procedure checkForm(i, k : Integer; var h2  : posCartes; var res, res2 : mem);
		procedure checkColor(i, k : Integer; var h2  : posCartes; var res, res2 : mem);
		procedure checkPossi(i, j, l, comp : Integer; var res, tam : mem; var pos : posCartes);
		procedure decPossiPlus();
		function getBestH(tam2 : mem; x, y : Integer; po : PNoeud) : mem;
		function getBestV(tam2 : mem; x, y : Integer; po : PNoeud) : mem;
	  end;


implementation
	uses Umain;



	constructor Computer.create(nom : String);
	begin
		inherited Create(nom);
	end;

	function Computer.play() : Boolean; // la fonction play imposé par la classe
	begin
		decPossiPlus();
		exit(True); 
	end;

	function Computer.getBestV(tam2 : mem; x, y : Integer; po : PNoeud) : mem;
	var tam, tam3 : mem;
		i, l, k : Integer;
		po2 : PNoeud;
		x2, y2 : Integer;
		pos : posPion;
		p : pion;
	begin
		x2 := x;
		y2 := y;
		po2 := po;
		tam.val := 0;
		k := 0;
		for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then k := k + 1;
		if (k > 0) then begin
			tam.val := 0;
			setlength(tam.ouysont, length(tam2.ouysont));
			setlength(tam.lespions, length(tam2.lespions));
			while po2^.value <> Nil do begin // on va en haut de la colonne
				po2 := getNoeud2(0, 1, po2);
				y2 := y2+1;
			end;
			for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then begin
				new(tam2.ouysont[i]);
				tam2.ouysont[i]^.x := x2;
				tam2.ouysont[i]^.y := y2;
				setlength(pos, 0);
				setlength(pos, length(tam2.ouysont));
				l := 0;
				for k := 0 to length(tam2.ouysont)-1 do if (tam2.ouysont[k] <> Nil) then begin
					new(pos[l]);
					pos[l]^ := tam2.ouysont[k]^;
					l := l + 1;
				end;
				if verifPlace(tam2.lespions[i]^,tam2.ouysont[i]^,pos) then begin // s'il peut poser il va faire un appel en récurrence 
					UMap.placePoint(x2, y2, tam2.lespions[i]^);
					p := tam2.lespions[i]^;
					dispose(tam2.lespions[i]);
					tam2.lespions[i] := Nil;
					tam3 := getBestV(tam2, x, y, po);
					if (tam.val < tam3.val) then begin
						tam.val := tam3.val;
						for k := 0 to length(tam.ouysont)-1 do begin
							if (tam.ouysont[k] <> Nil) then dispose(tam.ouysont[k]);
							if (tam3.ouysont[k] <> Nil) then begin
								new(tam.ouysont[k]);
								tam.ouysont[k]^.x := tam3.ouysont[k]^.x;
								tam.ouysont[k]^.y := tam3.ouysont[k]^.y;
							end;
						end;
					end;
					new(tam2.lespions[i]);
					tam2.lespions[i]^ := p;
					UMap.resetPoint(x2, y2);
				end;
				dispose(tam2.ouysont[i]);
				tam2.ouysont[i] := Nil;
			end;
			y2 := y;
			po2 := po;
			while po2^.value <> Nil do begin // on va en bas de la colonne
				po2 := getNoeud2(0, -1, po2);
				y2 := y2-1;
			end;
			for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then begin
				new(tam2.ouysont[i]);
				tam2.ouysont[i]^.x := x2;
				tam2.ouysont[i]^.y := y2;
				setlength(pos, 0);
				setlength(pos, length(tam2.ouysont));
				l := 0;
				for k := 0 to length(tam2.ouysont)-1 do if (tam2.ouysont[k] <> Nil) then begin
					new(pos[l]);
					pos[l]^ := tam2.ouysont[k]^;
					l := l + 1;
				end;
				if verifPlace(tam2.lespions[i]^,tam2.ouysont[i]^,pos) then begin // s'il peut poser il va faire un appel en récurrence 
					UMap.placePoint(x2, y2, tam2.lespions[i]^);
					p := tam2.lespions[i]^;
					dispose(tam2.lespions[i]);
					tam2.lespions[i] := Nil;
					tam3 := getBestV(tam2, x, y, po);
					if (tam.val < tam3.val) then begin
						tam.val := tam3.val;
						for k := 0 to length(tam.ouysont)-1 do begin
							if (tam.ouysont[k] <> Nil) then dispose(tam.ouysont[k]);
							if (tam3.ouysont[k] <> Nil) then begin
								new(tam.ouysont[k]);
								tam.ouysont[k]^.x := tam3.ouysont[k]^.x;
								tam.ouysont[k]^.y := tam3.ouysont[k]^.y;
							end;
						end;
					end;
					new(tam2.lespions[i]);
					tam2.lespions[i]^ := p;
					UMap.resetPoint(x2, y2);
				end;
				dispose(tam2.ouysont[i]);
				tam2.ouysont[i] := Nil;
			end;
		end else begin
			tam2.val := calcPoints(tam2.ouysont,length(tam2.ouysont))+1;
		end;
		if (tam.val > tam2.val) then
			getBestV := tam
		else
			getBestV := tam2;
	end;
	
	function Computer.getBestH(tam2 : mem; x, y : Integer; po : PNoeud) : mem;
	var tam, tam3 : mem;
		i, l, k : Integer;
		po2 : PNoeud;
		x2, y2 : Integer;
		pos : posPion;
		p : pion;
	begin
		x2 := x;
		y2 := y;
		po2 := po;
		tam.val := 0;
		k := 0;
		for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then k := k + 1;
		if (k > 0) then begin
			tam.val := 0;
			setlength(tam.ouysont, length(tam2.ouysont));
			setlength(tam.lespions, length(tam2.lespions));
			while po2^.value <> Nil do begin // on va à droite de la ligne
				po2 := getNoeud2(1, 0, po2);
				x2 := x2+1;
			end;
			for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then begin
				new(tam2.ouysont[i]);
				tam2.ouysont[i]^.x := x2;
				tam2.ouysont[i]^.y := y2;
				setlength(pos, 0);
				setlength(pos, length(tam2.ouysont));
				l := 0;
				for k := 0 to length(tam2.ouysont)-1 do if (tam2.ouysont[k] <> Nil) then begin
					new(pos[l]);
					pos[l]^ := tam2.ouysont[k]^;
					l := l + 1;
				end;
				if verifPlace(tam2.lespions[i]^,tam2.ouysont[i]^,pos) then begin
					UMap.placePoint(x2, y2, tam2.lespions[i]^);
					p := tam2.lespions[i]^;
					dispose(tam2.lespions[i]);
					tam2.lespions[i] := Nil;
					tam3 := getBestH(tam2, x, y, po);
					if (tam.val < tam3.val) then begin
						tam.val := tam3.val;
						for k := 0 to length(tam.ouysont)-1 do begin
							if (tam.ouysont[k] <> Nil) then dispose(tam.ouysont[k]);
							if (tam3.ouysont[k] <> Nil) then begin
								new(tam.ouysont[k]);
								tam.ouysont[k]^.x := tam3.ouysont[k]^.x;
								tam.ouysont[k]^.y := tam3.ouysont[k]^.y;
							end;
						end;
					end;
					new(tam2.lespions[i]);
					tam2.lespions[i]^ := p;
					UMap.resetPoint(x2, y2);
				end;
				dispose(tam2.ouysont[i]);
				tam2.ouysont[i] := Nil;
			end;
			x2 := x;
			po2 := po;
			while po2^.value <> Nil do begin // on va à gauche de la ligne
				po2 := getNoeud2(-1, 0, po2);
				x2 := x2-1;
			end;
			for i := 0 to length(tam2.lespions) - 1 do if (tam2.lespions[i] <> Nil) then begin
				new(tam2.ouysont[i]);
				tam2.ouysont[i]^.x := x2;
				tam2.ouysont[i]^.y := y2;
				setlength(pos, 0);
				setlength(pos, length(tam2.ouysont));
				l := 0;
				for k := 0 to length(tam2.ouysont)-1 do 
					if (tam2.ouysont[k] <> Nil) then begin
						new(pos[l]);
						pos[l]^ := tam2.ouysont[k]^;
						l := l + 1;
					end;
				if verifPlace(tam2.lespions[i]^,tam2.ouysont[i]^,pos) then begin // s'il peut poser il va faire un appel en récurrence 
					UMap.placePoint(x2, y2, tam2.lespions[i]^);
					p := tam2.lespions[i]^;
					dispose(tam2.lespions[i]);
					tam2.lespions[i] := Nil;
					tam3 := getBestH(tam2, x, y, po);
					if (tam.val < tam3.val) then begin
						tam.val := tam3.val;
						for k := 0 to length(tam.ouysont)-1 do begin
							if (tam.ouysont[k] <> Nil) then dispose(tam.ouysont[k]);
							if (tam3.ouysont[k] <> Nil) then begin
								new(tam.ouysont[k]);
								tam.ouysont[k]^.x := tam3.ouysont[k]^.x;
								tam.ouysont[k]^.y := tam3.ouysont[k]^.y;
							end;
						end;
					end;
					new(tam2.lespions[i]);
					tam2.lespions[i]^ := p;
					UMap.resetPoint(x2, y2);
				end;
				dispose(tam2.ouysont[i]);
				tam2.ouysont[i] := Nil;
			end;
		end else begin
			tam2.val := calcPoints(tam2.ouysont,length(tam2.ouysont))+1;
		end;
		if (tam.val > tam2.val) then
			getBestH := tam
		else
			getBestH := tam2;
	end;
	
// la porcédure qui va faire le test pour des coordonnées données
	procedure Computer.checkPossi(i, j, l, comp : Integer; var res, tam : mem; var pos : posCartes);
	var k : Integer;
	begin
		for k := 0 to length(tam.ouysont) - 1 do  // on réinitialise pour empécher des problème de mémoire
			if (tam.ouysont[k] <> Nil) then begin 
				dispose(tam.ouysont[k]); 
				tam.ouysont[k] := nil; 
			end;
		new(tam.ouysont[l]);
		tam.ouysont[l]^.x := i;
		tam.ouysont[l]^.y := j;
		if verifPlace(pos[l]^,tam.ouysont[l]^,tam.ouysont) then begin
			UMap.placePoint(i, j, pos[l]^); // on est obligé de poser le pion pour la fonction verif
			if comp = 1 then begin // s'il y a un seul pion à tester
				if res.val < calcPoints(tam.ouysont,comp)+1 then begin // on remplace
					res.val := calcPoints(tam.ouysont,comp)+1; // on met +1 pour que l'IA puisse jouer quand il n'y a rien
					for k := 0 to comp-1 do begin  
						if (res.ouysont[k] <> Nil) then dispose(res.ouysont[k]);
						new(res.ouysont[k]);
						res.ouysont[k]^.x := tam.ouysont[k]^.x;
						res.ouysont[k]^.y := tam.ouysont[k]^.y;
					end;
				end;	
			end else begin
				for k := 0 to length(tam.lespions) - 1 do begin
					if (tam.lespions[k] <> Nil) then dispose(tam.lespions[k]);
					tam.lespions[k] := Nil;
					if (pos[k] <> Nil) and (k <> l) then begin new(tam.lespions[k]); tam.lespions[k]^ := pos[k]^; end;								
				end;
				tam.val := 0;
				tam := getBestV(tam, i, j, UMap.getNoeud(i, j)); // apelle la fonction qui va tester par récurrence de poser en verticale
				if (res.val < tam.val) then begin// on remplace
					res.val := tam.val;
					for k := 0 to comp-1 do begin
						if (res.ouysont[k] <> Nil) then dispose(res.ouysont[k]);
						new(res.ouysont[k]);
						res.ouysont[k]^.x := tam.ouysont[k]^.x;
						res.ouysont[k]^.y := tam.ouysont[k]^.y;
					end;
				end;
				for k := 0 to length(tam.lespions) - 1 do begin // on se balade dans le tableau de la possibilité pour tester en changeant de pions de base
					if (tam.lespions[k] <> Nil) then dispose(tam.lespions[k]);
					tam.lespions[k] := Nil;
					if (pos[k] <> Nil) and (k <> l) then begin 
						new(tam.lespions[k]); 
						tam.lespions[k]^ := pos[k]^; 
					end;								
				end;
				tam.val := 0;
				tam := getBestH(tam, i, j, UMap.getNoeud(i, j)); // apelle la fonction qui va tester par récurrence de poser en horizontale
				if (res.val < tam.val) then begin // on remplace
					res.val := tam.val;
					for k := 0 to comp-1 do begin // on se balade dans le tableau de la possibilité pour tester en changeant de pions de base
						if (res.ouysont[k] <> Nil) then dispose(res.ouysont[k]);
						new(res.ouysont[k]);
						res.ouysont[k]^.x := tam.ouysont[k]^.x;
						res.ouysont[k]^.y := tam.ouysont[k]^.y;
					end;
				end;
			end;
			UMap.resetPoint(tam.ouysont[l]^.x, tam.ouysont[l]^.y); // on le supprime car ce n'est pas forcément le coup final
		end;
	end;
	
	// function qui rend la meilleur position pour des pions donnés en fonction de la val
	function Computer.testPoint(pos : posCartes) : mem;
	var aglhg, aglbd : pos; i, j, l, k, comp : integer; res, tam : mem;

	Begin
	comp := 0;
	for k := 0 to length(pos)-1 do // on calcule le nombre de pion dans la possibilité en cours
		if pos[k] <> Nil then comp := comp + 1;
	aglhg := UMap.getCoCoin(true,false);
	aglbd := UMap.getCoCoin(false,true);
	setlength(res.ouysont,comp);
	setlength(tam.ouysont,comp);
	setlength(tam.lespions,comp);
	res.val := 0;
	tam.val := 0;
	for k := 0 to length(res.ouysont)-1 do begin
		res.ouysont[k] := Nil;
		tam.ouysont[k] := Nil;
	end;
		i := Random(4); // cela va permettre de "randomiser" le caractère de l'IA
		if (i = 0) then
			for i := aglhg.x-1 to aglbd.x+1 do begin // on parcoure la map
				for j := aglbd.y-1 to aglhg.y+1 do begin
					for l := 0 to length(pos)-1 do if (pos[l] <> Nil) then begin
						checkPossi(i, j, l, comp, res, tam, pos);
					end;
				end;
			end
		else if (i = 1) then
			for i := aglhg.x-1 to aglbd.x+1 do begin
				for j := aglhg.y+1 downto aglbd.y-1 do begin
					for l := 0 to length(pos)-1 do if (pos[l] <> Nil) then begin
						checkPossi(i, j, l, comp, res, tam, pos);
					end;	
				end;
			end
		else if (i = 2) then
			for i := aglbd.x+1 downto aglhg.x-1 do begin
				for j := aglbd.y-1 to aglhg.y+1 do begin
					for l := 0 to length(pos)-1 do if (pos[l] <> Nil) then begin
						checkPossi(i, j, l, comp, res, tam, pos);
					end;
				end;
			end
		else if (i = 3) then
			for i := aglbd.x+1 downto aglhg.x-1 do begin
				for j := aglhg.y+1 downto aglbd.y-1 do begin
					for l := 0 to length(pos)-1 do if (pos[l] <> Nil) then begin
						checkPossi(i, j, l, comp, res, tam, pos);
					end;	
				end;
			end;
	res.lespions := pos;
	testPoint := res;
	End;

// trouve et applique la procedure de test sur toutes les possibilités avec la couleur en commun
	procedure Computer.checkColor(i, k : Integer; var h2  : posCartes; var res, res2 : mem);
	var j, l : Integer;
	begin
		for j := i + 1 to length(h2)-1 do begin 
				if h2[j]^.color = h2[i]^.color then begin
					if (res2.lespions[k] <> Nil) then dispose(res2.lespions[k]);
					new(res2.lespions[k]);			
					res2.lespions[k]^ := h2[j]^;
					res2 := testPoint(res2.lespions);
					if res2.val > res.val then begin
						res.val := res2.val;
						for l := 0 to length(res.lespions) - 1 do if res.lespions[l] <> Nil then dispose(res.lespions[l]);
						for l := 0 to length(res.ouysont) - 1 do if res.ouysont[l] <> Nil then dispose(res.ouysont[l]);
						setlength(res.ouysont, 0);
						setlength(res.lespions, 0);
						setlength(res.ouysont, length(res2.ouysont));
						setlength(res.lespions, length(res2.ouysont));
						for l := 0 to length(res.ouysont)-1 do begin
							res.lespions[l] := Nil;
							if (res2.lespions[l] <> Nil) then begin
								new(res.lespions[l]);
								res.lespions[l]^ := res2.lespions[l]^;
							end;
							res.ouysont[l] := Nil;
							if (res2.ouysont[l] <> Nil) then begin
								new(res.ouysont[l]);
								res.ouysont[l]^ := res2.ouysont[l]^;
							end;
						end;
					end;
					k := k +1;			
				end;
			end;
			for j := 1 to length(res2.lespions)-1 do 
				if (res2.lespions[j] <> Nil) then begin 
					dispose(res2.lespions[j]);
					res2.lespions[j] := nil;
				end;
	end;

// trouve et applique la procedure de test sur toutes les possibilités avec la forme en commun
	procedure Computer.checkForm(i, k  : Integer; var h2 : posCartes; var res, res2 : mem);
	var j, l : Integer;
	begin
			for j := i + 1 to length(h2)-1 do begin
				if h2[j]^.form = h2[i]^.form then begin
					if (res2.lespions[k] <> Nil) then dispose(res2.lespions[k]);
					new(res2.lespions[k]);
					res2.lespions[k]^ := h2[j]^;
					res2 := testPoint(res2.lespions);
					if res2.val > res.val then begin
						res.val := res2.val;
						for l := 0 to length(res.lespions) - 1 do if res.lespions[l] <> Nil then dispose(res.lespions[l]);
						for l := 0 to length(res.ouysont) - 1 do if res.ouysont[l] <> Nil then dispose(res.ouysont[l]);
						setlength(res.ouysont, 0);
						setlength(res.lespions, 0);
						setlength(res.ouysont, length(res2.ouysont));
						setlength(res.lespions, length(res2.ouysont));
						for l := 0 to length(res.ouysont)-1 do begin
							if (res.lespions[l] <> Nil) then dispose(res.lespions[l]);
							res.lespions[l] := Nil;
							if (res2.lespions[l] <> Nil) then begin
								new(res.lespions[l]);
								res.lespions[l]^ := res2.lespions[l]^;
							end;
							res.ouysont[l] := Nil;
							if (res2.ouysont[l] <> Nil) then begin
								new(res.ouysont[l]);
								res.ouysont[l]^ := res2.ouysont[l]^;
							end;
						end;
					end;
					k := k +1;
				end;
			end;
			for j := 1 to length(h2)-1 do 
			if (res2.lespions[j] <> Nil) then begin 
				dispose(res2.lespions[j]);
				res2.lespions[j] := nil;
			end;
	end;

//rend la meilleur possibilité à jouer pour une main
	procedure Computer.decPossiPlus();
	Var h : hand; res, res2 : mem; i, k, l : Integer; h1 : bout_sac; h2 : posCartes;

	Begin

		res.val := 0;
		h := getHand;
		h1 := nil;
		l := 0;
		for i := 0 to 5 do // transformation en liste chaine pour supprimer les doublons
			if (h[i] <> Nil) then begin
				if l = 0 then
					h1 := chaine.creerNoeud(h[i]^)
				else
					chaine.creerNoeudF(h[i]^,h1);
				l := l + 1;
			end;
		h1 := chaine.supprimerDoublons(h1);
		setlength(h2, chaine.longueur(h1));
		for i := 1 to chaine.longueur(h1) do begin
			new(h2[i-1]);
			h2[i-1]^ := chaine.rechercherK(h1,i)^;
		end;
		chaine.delete(h1);
		setlength(res2.lespions,length(h2));
		for i := 0 to length(h2)-1 do begin 
			k := 1;
			dispose(res2.lespions[0]);
			new(res2.lespions[0]);
			res2.lespions[0]^ := h2[i]^;
			res2 := testPoint(res2.lespions); // on appele la fonction qui va faire tout les tests
			if res2.val > res.val then begin // on remplace l'ancien coup par le nouveau s'il donne plus de points en empéchant les clones
				res.val := res2.val;
				for l := 0 to length(res.lespions) - 1 do if res.lespions[l] <> Nil then dispose(res.lespions[l]);
				for l := 0 to length(res.ouysont) - 1 do if res.ouysont[l] <> Nil then dispose(res.ouysont[l]);
				setlength(res.ouysont, 0);
				setlength(res.lespions, 0);
				setlength(res.ouysont, length(res2.ouysont));
				setlength(res.lespions, length(res2.lespions));
				for l := 0 to length(res.ouysont)-1 do begin
					if (res.lespions[l] <> Nil) then dispose(res.lespions[l]);
					res.lespions[l] := Nil;
					if (res2.lespions[l] <> Nil) then begin
						new(res.lespions[l]);
						res.lespions[l]^ := res2.lespions[l]^;
					end;
					res.ouysont[l] := Nil;
					if (res2.ouysont[l] <> Nil) then begin
						new(res.ouysont[l]);
						res.ouysont[l]^ := res2.ouysont[l]^;
					end;
				end;
			end;
			if Random(2) = 1 then begin // début de l'énumération des possibilitées de manière aléatoire 
				checkColor(i, k, h2, res, res2);
				checkForm(i, k, h2, res, res2);
			end else begin
				checkForm(i, k, h2, res, res2);
				checkColor(i, k, h2, res, res2);
			end;
		end;
		if (res.val = 0) then begin // gestion de la pioche s'il ne peut pas jouer
			if (longsac > 0) then begin
				i := 0;
				for l := 0 to 5 do begin
					for k := l + 1 to 5 do begin
						if (h[l] <> Nil) and (h[k] <> Nil) and (h[l]^ = h[k]^) then begin
							chaine.creerNoeudF(h[k]^, Utils.sac); 
							supprHand(h[k]^);
							i := i + 1;
						end;
					end;
				end;	
				if (i = 0) then begin
					while (i < 1) do begin
						l := Random(6);
						if (h[l] <> Nil) then begin
							chaine.creerNoeudF(h[l]^, Utils.sac); 
							supprHand(h[l]^);
							i := i + 1;
						end;
					end;
				end;
				pioche(i);
			end;
		end else begin
			setScore(getScore() + res.val - 1); // ajoute les points de l'IA 
			for i := 0 to length(res.ouysont)-1 do
				if (res.lespions[i] <> Nil) and (res.ouysont[i] <> Nil) then begin // pose les pions
					poser(res.lespions[i]^,res.ouysont[i]^.x,res.ouysont[i]^.y);
					pioche(1);
				end;
		end;
		
		for i := 0 to length(res.lespions) - 1 do if res.lespions[i] <> Nil then dispose(res.lespions[i]); // pour empécher de potentielles fuites de mémoires
		for i := 0 to length(res.ouysont) - 1 do if res.ouysont[i] <> Nil then dispose(res.ouysont[i]);
		for i := 0 to length(res2.lespions) - 1 do if res2.lespions[i] <> Nil then dispose(res2.lespions[i]);
		for i := 0 to length(res2.ouysont) - 1 do if res2.ouysont[i] <> Nil then dispose(res2.ouysont[i]);
		for i := 0 to length(h2) - 1 do if h2[i] <> Nil then dispose(h2[i]);
	End;

begin
end.

