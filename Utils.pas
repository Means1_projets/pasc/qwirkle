unit Utils;


interface

	Uses Crt, Glib2d;

	type
		Pion = record
			color : gColor;
			form : String;
		end;

		PPion = ^Pion;

		bout_sac = ^NoeudS;

		NoeudS = record
			valeur : Pion;
			suivant : bout_sac;
		end;

		hand = array[0..5] of PPion;

		PosCartes = array of PPion;
		Pos = record
			x,y: Integer;
		end;
		PPos = ^Pos;
		PosPion = array of PPos;

	//	map = array of array of pion;

		PNoeud = ^Noeud;
		Mem = record
			val : Integer;
			lespions : PosCartes;
			ouysont : PosPion;
		end;
		Noeud = Record
			value : PPion;
			up : PNoeud;
			down : PNoeud;
			right : PNoeud;
			left : PNoeud;
		end;

	procedure creationSac(c, f, t : Integer);
	function piocher(): PPion;
	function calcPoints(coord : PosPion; nbCartes : Integer) : Integer;
	function horizCompte(posit : PPos) : Integer;
	function vertCompte(posit : PPos) : Integer;
	function vQwirkle(posit : PPos; vert : Boolean; n : Integer) : Integer;

	operator = (x,y : gColor) r : Boolean;
	operator = (x,y : Pion) r : Boolean;
	
	var sac : bout_sac;
		longsac : Integer;


implementation
	uses chaine, UMap, UMain;

	operator = (x,y : gColor) r : Boolean;  
	begin  
		r := (x.r=y.r) and (x.g=y.g) and (x.b=y.b) and (x.a=y.a);  
	end; 

	operator = (x,y : Pion) r : Boolean;  
	begin  
		r := (x.color = y.color) and (x.form=y.form);  
	end; 



	procedure creationSac(c, f, t : Integer);
	var res : bout_sac; p : Pion; comp, i, j, k : Integer;	
	begin

	comp := 1;
	for i := 0 to t-1 do
	for j := 0 to c-1 do
	for k := 0 to f-1 do begin
		case j of
			0: p.color := COULEUR1;
			1: p.color := COULEUR2;
			2: p.color := COULEUR3;
			3: p.color := COULEUR4;
			4: p.color := COULEUR5;
			5: p.color := COULEUR6;
			6: p.color := COULEUR7;
			7: p.color := COULEUR8;
			8: p.color := COULEUR9;
			9: p.color := COULEUR10;
			10: p.color := COULEUR11;
			11: p.color := COULEUR12;
		end;
		case k of
			0: p.form := 'A';
			1: p.form := 'B';
			2: p.form := 'C';
			3: p.form := 'D';
			4: p.form := 'E';
			5: p.form := 'F';
			6: p.form := 'G';
			7: p.form := 'H';
			8: p.form := 'I';
			9: p.form := 'J';
			10: p.form := 'K';
			11: p.form := 'L';
		end;
		if (i = 0) and (j = 0) and (k = 0) then
			res := chaine.creerNoeud(p)
		else
			chaine.creerNoeudM(p, comp, res);
		comp := comp + 1;
	end;
	sac := res;
	end;

	//Pioche un pion dans le sac
	function piocher(): PPion;
	var al : Integer;
	var p : PPion;
	begin	
		p := Nil;
		longsac := longueur(sac);
		al := random(longsac) + 1;
		p := chaine.rechercherK(sac, al);
		if (p <> Nil) then begin
			if al = 1 then
				sac := chaine.supprimerNoeudD(sac)
			else if al = longsac then
				chaine.supprimerNoeudF(sac)
			else
				chaine.supprimerNoeudM(al, sac);
			longsac := longsac-1;
		end;
		exit(p);
	end;


	(* Calcul des points pour un tour *)

	//fonction principale de calcul des points
	function calcPoints(coord : PosPion; nbCartes : Integer) : Integer;
	var
		n, i : Integer;
	begin
		n := 0; //compteur de points
		n := n + horizCompte(coord[0]);
		n := n + vertCompte(coord[0]);
		if nbCartes>1 then
			if coord[0]^.y=coord[1]^.y then //pions placés horizontalement
				for i:=1 to nbCartes-1 do
					n := n + vertCompte(coord[i])
			else 
				for i:=1 to nbCartes-1 do
					n := n + horizCompte(coord[i]);
		calcPoints := n;	
	end;

	//compte le nombre de pions placés à l'horizontal d'un pion posit
	function horizCompte(posit : PPos) : Integer;
	var
		n, i : Integer;
	begin
		n := 0;
		i := 0;
		while UMap.getValueOrNil(posit^.x-i, posit^.y)<>nil do begin //compte le nombre de pions à gauche (en comptant le pion central)
			i := i+1;
			n := n+1;
		end;
		i := 1;
		while UMap.getValueOrNil(posit^.x+i, posit^.y)<>nil do begin //nombre de pions à droite (sans compter le pion central)
			i := i+1;
			n := n+1;
		end;
		n := vQwirkle(posit, false, n);
		if n = 1 then n:=0; //si le pion n'a pas de voisin, il ne marque pas de point en plus
		horizCompte := n;
	end;

	//compte le nombre de pions placés à la verticale du pion posit
	function vertCompte(posit : PPos) : Integer;
	var
		n, i : Integer;
	begin
		n := 0;
		i := 0;
		while UMap.getValueOrNil(posit^.x, posit^.y-i)<>nil do begin //nombre de pions en haut
			i := i+1;
			n := n+1;
		end;
		i := 1;
		while UMap.getValueOrNil(posit^.x, posit^.y+i)<>nil do begin //nombre de pions en bas
			i := i+1;
			n := n+1;
		end;
		n := vQwirkle(posit, true, n);
		if n = 1 then n:=0;
		vertCompte := n;
	end;

	//regarde s'il y a un Qwirkle
	function vQwirkle(posit : PPos; vert : Boolean; n : Integer) : Integer;
	var
		qwi : Boolean; //si c'est couleur(true) ou forme(false)
	begin
		if vert then //vertical
			if (* on vérifie avant que ces positions ne sont pas nulles pour que ça ne plante pas *)
			   (UMap.getValueOrNil(posit^.x, posit^.y) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x+1, posit^.y) <> Nil)
			   and (UMap.getValueOrNil(posit^.x, posit^.y)^.color = UMap.getValueOrNil(posit^.x+1, posit^.y)^.color) 
			   or (UMap.getValueOrNil(posit^.x-1, posit^.y) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x, posit^.y) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x, posit^.y)^.color=UMap.getValueOrNil(posit^.x-1, posit^.y)^.color) then //si le pion du dessus ou le pion du dessous est de la même couleur
				qwi := true
			else 
				qwi := false
		else //horizontal
			if (* on vérifie avant que ces positions ne sont pas nulles pour que ça ne plante pas *)
			   (UMap.getValueOrNil(posit^.x, posit^.y) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x, posit^.y+1) <> Nil)
			   and (UMap.getValueOrNil(posit^.x, posit^.y)^.color=UMap.getValueOrNil(posit^.x, posit^.y+1)^.color) 
			   or (UMap.getValueOrNil(posit^.x, posit^.y-1) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x, posit^.y) <> Nil) 
			   and (UMap.getValueOrNil(posit^.x, posit^.y)^.color=UMap.getValueOrNil(posit^.x, posit^.y-1)^.color) then
				qwi := true
			else
				qwi := false;
		if qwi = true then begin //qwirlkle de couleur 
			if UMain.ma.getNbCouleurs() = n then
				n := 2*n;
		end else //qwirkle de formes
			if UMain.ma.getNbFormes() = n then
				n := 2*n;
		vQwirkle := n;
	end;

begin
	randomize;
end.
