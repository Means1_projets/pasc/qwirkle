{$mode objfpc}
{$m+}
unit UPlayer;
interface

	uses Utils, chaine, UMap;
	//Class du player
	type Player = 	class

		private
			//Son nom
			name : String;
			//Sa main
			playerhand : hand;
			//Position des pions posé (pendant le tour)
			playedPos : posPion;
			//Pion posé (pendant le tour)
			playedPion : posCartes;
			//Son score
			score : integer;
		public
			constructor create(nom : String); // une function qui est appelée à la création de la classe
			function getName() : String;
			function getHand() : hand;
			function sizePlayedPos() : Integer;
			function sizePlayerHand() : Integer;
			function sizePlayedPion() : Integer;
			function getPlayedPos() : posPion;
			function getPlayedPion() : posCartes;
			function play() : Boolean; virtual; abstract; // Fonction abstract = les filles de Player doivent la définir, virtual : pas défini ici
			procedure pioche(i : Integer);
			procedure poser(p1 : pion; x, y : integer);
			procedure addHand(pi : pion);
			procedure cancel();
			procedure supprHand(pi : pion);
			procedure reputinSac(a : posCartes);
			function verifPlace(carte : pion; coord : pos; listCoord : posPion) : boolean;
			procedure setScore(i : integer);
			function getScore() : integer;
			procedure clearPosed();
			
	end;


implementation

	uses UMain;

		
	//nombre de pion posé
	function Player.sizePlayedPos() : Integer;
	var i : Integer;
	begin
		for i := 0 to 6 do begin
			if ((i < 6) and (getPlayedPos()[i] = Nil)) then begin
				break;
			end;
		end;
		exit(i);
	end;
	
	//nombre de pion utilisé
	function Player.sizePlayedPion() : Integer;
	var i : Integer;
	begin
		for i := 0 to 6 do begin
			if ((i < 6) and (getPlayedPion()[i] = Nil)) then begin
				break;
			end;
		end;
		exit(i);
	end;
	
	//nombre de pion dans la main
	function Player.sizePlayerHand() : Integer;
	var i, j : Integer;
	begin
		j := 0;
		for i := 0 to 5 do begin
			if (getHand()[i] <> Nil) then begin
				j := j + 1;
			end;
		end;
		exit(j);
	end;

	function Player.getPlayedPos() : posPion;
	begin
		exit(self.playedPos);
	end;


	function Player.getPlayedPion() : posCartes;
	begin
		exit(self.playedPion);
	end;

	
	procedure Player.setScore(i : integer);
	begin
		self.score := i;
	end;

	function Player.getScore() : integer;
	begin
		exit(self.score);
	end;
	
	//Reset
	procedure Player.clearPosed();
	var
		i : integer;
	begin
		for i := 0 to 5 do begin
			self.playedPos[i] := Nil;
			self.playedPion[i] := Nil;
		end;
	end;

	//Annule le tour
	procedure Player.cancel();
	var i : Integer;
	begin
		for i := 0 to length(playedPion)-1 do begin
			if (playedPion[i] <> Nil) then begin
				//Redonne le pion
				addHand(playedPion[i]^);
				//Enleve le pion
				if (playedPos[i] <> Nil) then 
					UMap.resetPoint(playedPos[i]^.x, playedPos[i]^.y);
				end;
		end;
		clearPosed();
		UMain.ma.setSelectPion(Nil);
	end;

	//vérifie qu'un pion est plaçable à un endroit
	function Player.verifPlace(carte : pion; coord : pos; listCoord : posPion) : boolean;
	var
		p1, p2 : Pnoeud;
		x, y, i, j, max, count, size : Integer;
		isx, isy, a : boolean;
		cartes : posCartes;
	var po1, po2 : pos;
	begin
		isx := False;
		isy := False;
		size := 0;
		for i := 0 to length(listCoord)-1 do begin
			if listCoord[i] <> Nil then begin
				size := size + 1;
			end;
		end;
		//Coin en haut à gauche
		po1 := getCoCoin(True, False);
		//Coin en bas à droite
		po2 := getCoCoin(False, True);
		//Récupère le noeud ou le joueur veut le placer
		p1 := UMap.getNoeudOrNil(coord.x, coord.y);
		if (p1 = Nil) then begin
			//Check si il le place dans un coin inaccesible
			if (((coord.x > po2.x) and (coord.y > po1.y)) or ((coord.x > po2.x) and (coord.y < po2.y)) or ((coord.x < po1.x) and (coord.y > po1.y)) or ((coord.x < po1.x) and (coord.y < po2.y))) THEN
				exit(false);
			//Check si il est en dehors de la map
			if (coord.x-1 > po2.x) or (coord.x+1 < po1.x) or (coord.y-1 > po1.y) or (coord.y+1 < po2.y) then exit(False);
			//Si il pose dans un endroit non généré
			if (coord.x > po2.x) then begin
				//Récupère le noeud à côté qui est généré et verif 
				p2 := UMap.getNoeud(po2.x, coord.y);
				if (p2^.value = Nil) then exit(False);
				while ((p2 <> nil) and (p2^.value <> nil)) do begin
					if (((p2^.value^.form <> carte.form) and (p2^.value^.color <> carte.color)) or  ((p2^.value^.form = carte.form) and (p2^.value^.color = carte.color))) then
						exit(false);
					p2 := p2^.left;
				end;
			end;
			if (coord.x < po1.x) then begin	
				//Récupère le noeud à côté qui est généré et verif 
				p2 := UMap.getNoeud(po1.x, coord.y);
				if (p2^.value = Nil) then exit(False);
				while ((p2 <> nil) and (p2^.value <> nil)) do begin	
					if (((p2^.value^.form <> carte.form) and (p2^.value^.color <> carte.color)) or  ((p2^.value^.form = carte.form) and (p2^.value^.color = carte.color))) then
						exit(false);
					p2 := p2^.right;
				end;
			end;
			if (coord.y > po1.y) then begin
				//Récupère le noeud à côté qui est généré et verif 
				p2 := UMap.getNoeud(coord.x, po1.y);
				if (p2^.value = Nil) then exit(False);
				while ((p2 <> nil) and (p2^.value <> nil)) do begin
					if (((p2^.value^.form <> carte.form) and (p2^.value^.color <> carte.color)) or  ((p2^.value^.form = carte.form) and (p2^.value^.color = carte.color))) then
					exit(false);
				p2 := p2^.down;
				end;
			end;
			if (coord.y < po2.y) then begin
				//Récupère le noeud à côté qui est généré et verif 
				p2 := UMap.getNoeud(coord.x, po2.y);	
				if (p2^.value = Nil) then exit(False);
				while ((p2 <> nil) and (p2^.value <> nil)) do begin
					if (((p2^.value^.form <> carte.form) and (p2^.value^.color <> carte.color)) or  ((p2^.value^.form = carte.form) and (p2^.value^.color = carte.color))) then
						exit(false);
					p2 := p2^.up;
				end;
			end;
		end;
		
		//Check si les pions sont aligné
		for i := 0 to length(listCoord)-1 do begin
			if (listCoord[i] = Nil) then break;
			if (i = 0) then begin
				x := listCoord[i]^.x;
				y := listCoord[i]^.y;
			end else if ((i = 1) and (x = listCoord[i]^.x)) then begin
				isx := True;
			end else if ((i = 1) and (y = listCoord[i]^.y)) then begin
				isy := True;
			end else if (i = 1) then exit(false)
			else if ((i > 1) and ((isx and (x <> listCoord[i]^.x)) or (isy and (y <> listCoord[i]^.y)))) then begin
				exit(false);
			end;
		end;
	
		if (p1 <> Nil) then begin
			if (Umain.ma.getNbCouleurs() > Umain.ma.getNbFormes()) then
				max := Umain.ma.getNbCouleurs()
			else max := Umain.ma.getNbFormes();
			setlength(cartes, max);
			count := 0;
			
			
			
			
			//Si pas de voisin ou pas en pos 0 0 -> exit
			if (p1^.value <> nil) or (((p1^.up = nil) or (p1^.up^.value = nil)) and ((p1^.right = nil) or (p1^.right^.value = nil)) and ((p1^.down = nil) or (p1^.down^.value = nil)) and ((p1^.left = nil) or (p1^.left^.value = nil)) and ((coord.x <> 0) or (coord.y <> 0))) then
				exit(false);
			p2 := p1;
			a := False;
			x := coord.x;
			y := coord.y;
			//Se place sur la gauche (non nul)
			while ((p2^.left <> nil) and (p2^.left^.value <> nil)) do begin
				p2 := p2^.left;
				x := x-1;
			end;
			for i := 0 to max-1 do cartes[i] := Nil;
			j := 0;
			//Se déplace vers la droite et check si tous les points sont sur la meme ligne
			while (((p2 <> nil) and (p2^.value <> nil)) or ((a = False) and (p2 <> Nil))) do begin
				for i := 0 to length(listCoord)-1 do
					if (listCoord[i] <> Nil) and (listCoord[i]^.x = x) and (listCoord[i]^.y = y) then begin
						count := count + 1;
					end;
				if (p2^.value = Nil) and (a = False) then begin
					for i := 0 to max-1 do
						if (cartes[i] <> Nil) and (((carte.form <> cartes[i]^.form) and (carte.color <> cartes[i]^.color)) or  ((carte.form = cartes[i]^.form) and (carte.color = cartes[i]^.color))) then
							exit(false);
					new(cartes[j]);
					cartes[j]^ := carte;
					a := True;
				end else begin
					for i := 0 to max-1 do 
						if (cartes[i] <> Nil) and (((p2^.value^.form <> cartes[i]^.form) and (p2^.value^.color <> cartes[i]^.color)) or  ((p2^.value^.form = cartes[i]^.form) and (p2^.value^.color = cartes[i]^.color))) then
							exit(false);
					new(cartes[j]);
					cartes[j]^ := p2^.value^;
				end;
				p2 := p2^.right;
				j := j+1;
				x := x+1;
			end;
			if ((isy) and (count <> size)) then exit(False);
			p2 := p1;
			x := coord.x;
			y := coord.y;
			//Se place sur le haut (non nul)
			while ((p2^.up <> nil) and (p2^.up^.value <> nil)) do begin
				p2 := p2^.up;
				y := y+1;
			end;
			a := False;
			for i := 0 to max-1 do cartes[i] := Nil;
			count := 0;
			j := 0;
			//Se déplace vers la bas et check si tous les points sont sur la meme colonne
			while (((p2 <> nil) and (p2^.value <> nil)) or ((a = False) and (p2 <> Nil))) do begin
				for i := 0 to length(listCoord)-1 do
					if (listCoord[i] <> Nil) and (listCoord[i]^.x = x) and (listCoord[i]^.y = y) then begin
						count := count + 1;
					end;
				if (p2^.value = Nil) and (a = False) then begin
					for i := 0 to max-1 do
						if (cartes[i] <> Nil) and (((carte.form <> cartes[i]^.form) and (carte.color <> cartes[i]^.color)) or  ((carte.form = cartes[i]^.form) and (carte.color = cartes[i]^.color))) then
							exit(false);
					new(cartes[j]);
					cartes[j]^ := carte;
					a := True;
				end else begin
					for i := 0 to max-1 do
						if (cartes[i] <> Nil) and (((p2^.value^.form <> cartes[i]^.form) and (p2^.value^.color <> cartes[i]^.color)) or  ((p2^.value^.form = cartes[i]^.form) and (p2^.value^.color = cartes[i]^.color))) then
							exit(false);
					new(cartes[j]);
					cartes[j]^ := p2^.value^;
				end;
				p2 := p2^.down;
				j := j+1;
				y := y-1;
			end;
			if ((isx) and (count <> size)) then exit(False);
		end;
		verifPlace := true;
	end;

	//crée un joueur
	constructor Player.create(nom : String);
	var i : Integer;
	begin
		self.name := nom;
		for i := 0 to length(self.playerhand)-1 do
				self.playerhand[i] := Nil;
		self.score := 0;
		setlength(playedPos, 6);
		setlength(playedPion, 6);
		clearPosed();
	end;

	//supprime la main d'un joueur
	procedure Player.supprHand(pi : pion);
	var j : Integer;

	begin

		for j := 0 to length(getHand)-1 do
			if ((self.playerhand[j] <> Nil) and (self.playerhand[j]^.color = pi.color) and (self.playerhand[j]^.form = pi.form)) then begin
				dispose(self.playerhand[j]);
				self.playerhand[j] := Nil;
				break;
			end;

	end;

	//remet des cartes dans le sac
	procedure Player.reputinSac(a : posCartes);
	var i : Integer;
	begin
		for i := 0 to length(a)-1 do begin
			if (a[i] <> Nil) then begin
				chaine.creerNoeudF(a[i]^, Utils.sac);
			end;
		end;
	end;

	//pioche i cartes
	procedure Player.pioche(i : Integer);
	var j : Integer;
	var p : Ppion;
	begin
		for j := 0 to i-1 do begin
			p := Utils.piocher();
			if (p <> Nil) then 
				addHand(p^);
		end;
	end;

	//ajoute une carte dans la main
	procedure Player.addHand(pi : pion);
	var i : Integer;
	begin
		for i := 0 to length(self.playerhand)-1 do
			if (self.playerhand[i] = Nil) then begin
				new(self.playerhand[i]);
				self.playerhand[i]^ := pi;
				break;
			end;
	end;

	//pose le pion p1 aux coordonnées x, y
	procedure Player.poser(p1 : pion; x, y : integer);
	begin

	supprHand(p1);
	UMap.placePoint(x, y, p1);
	end;


	//donne le nom du joueur
	function Player.getName() : String;
	begin
		exit(self.name);
	end;

	//rend la main du joueur
	function Player.getHand() : hand;
	begin
		exit(self.playerhand);
	end;




begin
end.
