{$mode objfpc}
{$m+}
unit UMain;

interface

	uses Utils, UMap, SDL,  UPlayer, UHuman, UComputer, Glib2d, SDL_TTF, sysutils;

	type
		//List de Joueurs 
		listPlayer = array of Player;

		//La class Main : gère le jeu
		Main = 	class
		//Variables private
		private
			//Paramètres
			nbJoueurs, nbFormes, nbCouleurs, nbPieces : Integer;
			//Joueurs
			players : ListPlayer;
			//Pion sélectionné (graphique)
			selectPion : PPion;
			//Pos cliqué (graphique)
			selectPos : PPos;
			//Si click sur validé ou pioché
			valid, piocher, fini : Boolean;
		//Fonctions public (accessible depuis d'autres fichiers
		public
			constructor create(nbJoueur, nbForme, nbCouleur, nbPiece : Integer);
			procedure play(nbJoueur : String);
			procedure createPlayers(nbJoueur : String);
			procedure addPlayer(pl : Player);
			procedure update(p : Player);
			procedure printMap();
			procedure updateScreen(p : Player);
			function getSelectPion() : PPion;
			function getValid() : Boolean;
			function getPioche() : Boolean;
			procedure setSelectPion(p : PPion);
			function getSelectPos() : PPos;
			function getNbCouleurs() : Integer;
			function getNbFormes() : Integer; 
			function getNbPieces() : Integer;
			procedure finish();
	end;

	procedure create(nbJoueur : String; nbForme, nbCouleur, nbPiece : Integer);

	var
		//Instance du jeu
		ma : Main;
		//Quand il clique = vrai
		click : Boolean;
		font : PTTF_Font;
		//Bouton stocké pour pas les recharger
		check, cancel, pioche, planche : gImage;
implementation

	(* FONCTIONS POUR RECUPERER LES VARIABLES DANS LES AUTRES UNITS *)

	function Main.getNbCouleurs() : Integer;
	begin
		exit(self.nbCouleurs);
	end;

	function Main.getNbFormes() : Integer; 
	begin
		exit(self.nbFormes);
	end;

	function Main.getNbPieces() : Integer;
	begin
		exit(self.nbPieces);
	end;

	function Main.getValid() : Boolean;
	begin
		exit(self.valid);
	end;

	function Main.getPioche() : Boolean;
	begin
		exit(self.piocher);
	end;

	procedure Main.setSelectPion(p : PPion);
	begin
		self.selectPion := p;
	end;
	
	procedure Main.finish();
	begin
	end;

	function Main.getSelectPion() : PPion;
	begin
		exit(self.selectPion);
	end;

	function Main.getSelectPos() : PPos;
	begin
		exit(self.selectPos);
	end;

	//Initialise les variables par défaut : constructeur -> première fonction éxecuté pour créer une class.
	constructor Main.create(nbJoueur, nbForme, nbCouleur, nbPiece : Integer);
	begin
		self.nbJoueurs := nbJoueur;
		self.nbFormes := nbForme;
		self.nbCouleurs := nbCouleur;
		self.nbPieces := nbPiece;
		self.valid := False;
		self.piocher := False;
		selectPion := nil;
	end;

	//Met a jour l'écran en fonction du joueur (bouton, sa main, nom, score)
	procedure Main.updateScreen(p : Player);
	var	playerHand : Hand;
		i, j, k : Integer;
		text: gImage;
	begin
		//Vrai quand le joueur est un humain
		if (p.ClassType.InheritsFrom(Human)) then begin
			j := p.sizePlayedPion();
			k := p.sizePlayedPos();
			gBeginRects(planche); (* No texture *)
				gSetScaleWH(321, 76);
				gSetCoord(20,612);
				gAdd();
			gEnd();
			if (j > 0) then begin
				//Place le bouton validé
				gBeginRects(check); (* No texture *)
					gSetScaleWH(40, 40);
					gSetCoord(480,630);
					gAdd();
				gEnd();
				//Place le bouton annulé
				gBeginRects(cancel);
					gSetScaleWH(40, 40);
					gSetCoord(560,630);	
					gAdd();
				gEnd();	
			end;
			if (k = 0) and (j < 6) and (longsac > j) then begin
				//Place le bouton de la pioche
				gBeginRects(pioche);
					gSetScaleWH(40, 40);
					gSetCoord(400,630);
					gAdd();
				gEnd();
			end;
			//Récupère sa main
			if ((sdl_get_mouse_y > 630) and (sdl_get_mouse_y < 670) and (sdl_get_mouse_x > 60) and (sdl_get_mouse_x < 300)) then begin
				playerHand := p.getHand();
				for i := 0 to length(playerHand)-1 do begin
					if (playerHand[i] <> Nil) then begin
						//Affiche sa main (color)
						gBeginRects(nil); (* No texture *)	
							gSetColor(playerHand[i]^.color);
							gSetScaleWH(40, 40);
							gSetCoord(i*40+60, 630);
							gAdd();
						gEnd();
						//Affiche sa main (text)
						text := gTextLoad(playerHand[i]^.form, font);
						gBeginRects(text);
							gSetColor(WHITE);
							gSetCoord(i*40+74, 640);
							gAdd();	
						gEnd();
						gTexFree(text);
					end;
				end;
			end;
		end;
		(* affichage du nom *)
		text := gTextLoad(p.getName(), font);
		gBeginRects(text);
			gSetColor(WHITE);
			gSetCoord(15, 15); //abscisse, ordonnée
			gAdd();
		gEnd();
		gTexFree(text);
		(* affichage du score *)
		text := gTextLoad('Score : '+IntToStr(p.getScore()), font);
		gBeginRects(text);
			gSetColor(WHITE);
			gSetCoord(15, 35); //abscisse, ordonnée
			gAdd();
		gEnd();
		gTexFree(text);
	end;

	//rafraichit tout le temps la page (sans forcément en changer le contenu)
	procedure Main.update(p : Player);
	var i, c1, c2 : Integer;
		po1, po2 : Pos;
		text: gImage;
	begin
		valid := False;
		piocher := False;
		if (selectPos <> Nil) THEn begin
			dispose(selectPos);
			selectPos := Nil;
		end;
		//Vrai si il click et si c'est un humain
		if ((click) and (p.ClassType.InheritsFrom(Human))) then begin
			//Zone de la map
			if ((sdl_get_mouse_x >= 0) and (sdl_get_mouse_y >= 0) and (sdl_get_mouse_x <= 600) and (sdl_get_mouse_y <= 600) and (selectPion<>nil)) then begin
				po1 := UMap.getCoCoin(True, False); //pion en haut à gauche
				po2 := UMap.getCoCoin(False, True); //pion en bas à droite
				c1 := (po2.x - po1.x + 3);
				c2 := (po1.y - po2.y + 3);
				if (c2 > c1) then c1 := c2;
				i := trunc(600/c1);
				new(selectPos);
				selectPos^.x := Trunc(sdl_get_mouse_x/i)+po1.x-1;
				selectPos^.y := -Trunc(sdl_get_mouse_y/i)+po1.y+1;
			//Zone de la main du joueur
			end else if ((sdl_get_mouse_y > 630) and (sdl_get_mouse_y < 670) and (sdl_get_mouse_x > 60) and (sdl_get_mouse_x < 300)) then
				selectPion := p.getHand()[trunc((sdl_get_mouse_x-60)/40)]
			//Bouton validé
			else if ((sdl_get_mouse_y > 630) and (sdl_get_mouse_y < 670) and (sdl_get_mouse_x > 480) and (sdl_get_mouse_x < 520)) then
				valid := True
			//Bouton annulé
			else if ((sdl_get_mouse_y > 630) and (sdl_get_mouse_y < 670) and (sdl_get_mouse_x > 560) and (sdl_get_mouse_x < 600)) then
				p.cancel()
			//Bouton piocher
			else if ((sdl_get_mouse_y > 630) and (sdl_get_mouse_y < 670) and (sdl_get_mouse_x > 400) and (sdl_get_mouse_x < 440)) then
				piocher := True;
		end;
		while (sdl_update = 1) do begin
			if (sdl_do_quit) then halt;
			if sdl_mouse_left_down then click:= true;
			if sdl_mouse_left_up then click:= false;
		end;
		updateScreen(p);
		
		if selectPion<>nil then begin
			//Affiche la pièce sélectionné
			gBeginRects(nil); (* No texture *)
				gSetColor(selectPion^.color);
				gSetScaleWH(40, 40);
				gSetCoord(sdl_get_mouse_x-20, sdl_get_mouse_y-20);
				gAdd();
			gEnd();
			text := gTextLoad(selectPion^.form, font);
			gBeginRects(text);
				gSetColor(WHITE);
				gSetCoord(sdl_get_mouse_x-6, sdl_get_mouse_y-10);
				gAdd();
			gEnd();
			gTexFree(text);
		end;
	end;

	//Créer les joueurs
	procedure Main.createPlayers(nbJoueur : String);
	var i, j, k : Integer;
		text: gImage;
		name : String;
		enter, maj : Boolean;
	begin
		setLength(players, nbJoueurs);
		j := 1;
		k := 1;
		for i := 1 to nbJoueurs do begin
			if (nbJoueur[i] = 'o') then begin
				addPlayer(Computer.create('Ordi '+ IntToStr(j)));
				j := j + 1;
			end else if (nbJoueur[i] = 'h') then begin
				enter := False;
				name := ' ';
				maj := False;
				while enter = False do begin
					gClear(WHITE);
					text := gTextLoad('Joueur '+IntToStr(k)+', Votre nom : ', font);
					gBeginRects(text);
						gSetColor(BLACK);
						gSetCoord(220, 300);
						gAdd();
					gEnd();
					gTexFree(text);
					text := gTextLoad(name, font);
					gBeginRects(text);
						gSetColor(BLACK);
						gSetCoord(300-length(name)*4, 400);
						gAdd();
					gEnd();
					gTexFree(text);
					gFlip();
					while (sdl_update = 1) do begin
						if (sdl_do_quit) then
							halt;
						case sdl_get_keypressed of
							SDLK_A : if maj then name := name + 'A' else name := name + 'a';
							SDLK_B : if maj then name := name + 'B' else name := name + 'b';
							SDLK_C : if maj then name := name + 'C' else name := name + 'c';
							SDLK_D : if maj then name := name + 'D' else name := name + 'd';
							SDLK_E : if maj then name := name + 'E' else name := name + 'e';
							SDLK_F : if maj then name := name + 'F' else name := name + 'f';
							SDLK_G : if maj then name := name + 'G' else name := name + 'g';
							SDLK_H : if maj then name := name + 'H' else name := name + 'h';
							SDLK_I : if maj then name := name + 'I' else name := name + 'i';
							SDLK_J : if maj then name := name + 'J' else name := name + 'j';
							SDLK_K : if maj then name := name + 'K' else name := name + 'k';
							SDLK_L : if maj then name := name + 'L' else name := name + 'l';
							SDLK_M : if maj then name := name + '?' else name := name + 'm';
							SDLK_SEMICOLON  : if maj then name := name + '?' else name := name + ',';
							SDLK_N : if maj then name := name + 'N' else name := name + 'n';
							SDLK_O : if maj then name := name + 'O' else name := name + 'o';
							SDLK_P : if maj then name := name + 'P' else name := name + 'p';
							SDLK_Q : if maj then name := name + 'A' else name := name + 'q';
							SDLK_R : if maj then name := name + 'R' else name := name + 'r';
							SDLK_S : if maj then name := name + 'S' else name := name + 's';
							SDLK_T : if maj then name := name + 'T' else name := name + 't';
							SDLK_U : if maj then name := name + 'U' else name := name + 'u';
							SDLK_V : if maj then name := name + 'V' else name := name + 'v';
							SDLK_W : if maj then name := name + 'W' else name := name + 'w';
							SDLK_COMMA : if maj then name := name + '.' else name := name + ';';
							SDLK_PERIOD : if maj then name := name + '/' else name := name + ':';
							SDLK_SLASH : if maj then name := name + '§' else name := name + '!';
							SDLK_X : if maj then name := name + 'X' else name := name + 'x';
							SDLK_Y : if maj then name := name + 'Y' else name := name + 'y';
							SDLK_Z : if maj then name := name + 'Z' else name := name + 'z';
							SDLK_SPACE : name := name + ' ';
							SDLK_1 : if maj then name := name + '&' else name := name + '1';
							SDLK_2 : if maj then name := name + 'é' else name := name + '2';
							SDLK_3 : if maj then name := name + '"' else name := name + '3';
							SDLK_4 : if maj then name := name + '''' else name := name + '4';
							SDLK_5 : if maj then name := name + '(' else name := name + '5';
							SDLK_6 : if maj then name := name + '-' else name := name + '6';
							SDLK_7 : if maj then name := name + 'è' else name := name + '7';
							SDLK_8 : if maj then name := name + '_' else name := name + '8';
							SDLK_9 : if maj then name := name + 'ç' else name := name + '9';
							SDLK_0 : if maj then name := name + 'à' else name := name + '0';
							SDLK_BACKSPACE : if length(name) > 1 then name := LeftStr(name, length(name)-1);
							SDLK_CAPSLOCK : maj := (maj = False);
							SDLK_RETURN  : if length(name) > 1 then enter := True;
						end;
					end;
				end;
				addPlayer(Human.create(RightStr(name, length(name)-1)));
				k := k + 1;
			end;
		end;
	end;

	//Affiche la map
	procedure Main.printMap();
	var po1, po2 : Pos;
		k, l, c1, c2, i, c  : Integer;
		no, no2 : PNoeud;
		font : PTTF_Font;
		text : gImage;
	begin
		po1 := UMap.getCoCoin(True, False);
		po2 := UMap.getCoCoin(False, True);
		no := UMap.getCoin(True, False);
		c1 := (po2.x - po1.x + 3);
		c2 := (po1.y - po2.y + 3);
		if (c2 > c1) then c1 := c2;
		i := trunc(600/c1);
		c := 0;
		font := TTF_OpenFont('font.ttf', i);
		for k := 0 to (c1-1)*2 do begin
			no2 := no;
			for l := 0 to (c1-1)*2 do begin
				//Vide
				if ((l = 0) or (k = 0) or (no2 = Nil) or (no2^.value = Nil)) then  begin
					if (c mod 2 = 0) then begin
						gBeginRects(nil); (* No texture *)
							gSetColor(BLACK);
							gSetScaleWH(i, i);
							gSetCoord(l*i, k*i);
							gAdd();
						gEnd();
					end
					else begin
						gBeginRects(nil); (* No texture *)
							gSetColor(GRAY);
							gSetScaleWH(i, i);
							gSetCoord(l*i, k*i);
							gAdd();
						gEnd();
					end;
				//Non vide
				end else begin
					gBeginRects(nil); (* No texture *)
						gSetColor(no2^.value^.color);
						gSetScaleWH(i, i);
						gSetCoord(l*i, k*i);
						gAdd();
					gEnd();
					text := gTextLoad(no2^.value^.form, font);
					gBeginRects(text);
						gSetColor(WHITE);
						gSetCoord(l*i+i/4, k*i);
						gAdd();
					gEnd();
					gTexFree(text);
				end;
				c := c + 1;
				if ((l > 0) and (k > 0) and (no2 <> Nil)) then no2 := no2^.right;
			end;
			if ((k > 0) and (no <> Nil)) then no := no^.down;
		end;
		TTF_CloseFont(font);
	end;
	
	//Function principale
	procedure Main.play(nbJoueur : String);
	var i, j, s, k : Integer;
		text : gImage;
	begin
		self.fini := False;
		//Valeur par défaut
		gClear(WHITE);
		font := TTF_OpenFont('font.ttf', 20);
		check := gTexLoad('check.png');
		cancel := gTexLoad('cancel.png');
		planche := gTexLoad('planche.png');
		pioche := gTexLoad('pioche.png');
		//Creer la map
		UMap.createMain();
		//Creer le sac
		Utils.creationSac(nbCouleurs, nbFormes, nbPieces);
		//Creer les joueurs
		createPlayers(nbJoueur);
		i := 0;
		//Boucle principale
		while self.fini = False do begin
			//Efface l'écran
			gClear(WHITE);
			//Affiche un rectangle vert
			printMap();
			gBeginRects(nil); (* No texture *)
				gSetColor(BOUTEILLE);
				gSetScaleWH(700, 100);
				gSetCoord(0, 600);
				gAdd();
			gEnd();
			//Affiche la map
			//Affiche les caractéristique du joueur
			update(players[i]);
			//Update l'écran
			gFlip();
			//Si il joue, suivant
			if (players[i].play()) then begin
				if (players[i].sizePlayerHand() = 0) then begin self.fini := True; players[i].setScore(players[i].getScore()); end;
				i := (i+1) mod length(players);
			end;
		end;
		gClear(WHITE);
		gBeginRects(nil); (* No texture *)
			gSetColor(BOUTEILLE); //Remplissage du fond en vert bouteille
			gSetScaleWH(600, 700);
			gSetCoord(0, 0);
			gAdd();
		gEnd();
		text := gTextLoad('Le jeu est fini ! Voici les scores finaux : ', font);
		gBeginRects(text);
			gSetColor(WHITE);
			gSetCoord(100, 100);
			gAdd();
		gEnd();
		gTexFree(text);
		for i := 0 to length(players) - 1 do begin //affiche les scores de chaque joueur du gagnant au perdant
			s := 0;
			for j := 0 to length(players) - 1 do begin //boucle qui récupère le score du meilleur joueur
				if (players[j] <> Nil) and (players[j].getScore() > s) then begin
					s := players[j].getScore();
					k := j; // k : pour savoir de quel joueur il s'agit
				end;
			end;
			//met dans la string text le rang du joueur, son nom et son nombre de points
			if i = 0 then begin//pour le 1er 
				text := gTextLoad(IntToStr(i+1) + 'er : ' + players[k].getName() + ' avec ' + IntToStr(players[k].getScore()) + ' points   	 Bravo champion ! 	 ', font); //les tab font des espèces de vaisseaux spaciaux
			end else text := gTextLoad(IntToStr(i+1) + 'eme : ' + players[k].getName() + ' avec ' + IntToStr(players[k].getScore()) + ' points', font);
			gBeginRects(text);
				gSetColor(WHITE);
				gSetCoord(100, 100 + (i+1) * 40);
				gAdd();
			gEnd();
			gTexFree(text); //affiche text
			players[k] := Nil; //on efface le joueur dont on a déjà affiché le score (la prochaine fois la boucle trouvera le premier des joueurs exeptés k, donc le joueur après k
		end;
		gFlip();
		while true do begin
			while (sdl_update = 1) do begin 
				if (sdl_do_quit) then halt; //si le joueur clique sur la croix on quitte
			end;
			
		end;
	end;

	//Ajouter un joueur (et le créer)
	procedure Main.addPlayer(pl : Player);
	var i : Integer;
	begin
		for i := 0 to length(players) do
			if players[i] = Nil then begin
				players[i] := pl;
				players[i].pioche(6);
				break;
			end;

	end;
	
	//Fonction principale
	procedure create(nbJoueur : String; nbForme, nbCouleur, nbPiece : Integer);
	begin
			ma := Main.create(length(nbJoueur), nbForme, nbCouleur, nbPiece);
			ma.play(nbJoueur);

	end;



begin
end.
