{$mode objfpc}
{$m+}
unit UHuman;
interface
uses Utils, UPlayer, crt, UMap;

type
	//Class de l'humain
	Human = 	class(Player) //Extends de Player
	public
    	constructor create(nom : String);
		function play() : Boolean; override; //override -> function imposé
  	end;

implementation

uses UMain;

//Constructeur
constructor Human.create(nom : String);
begin
	inherited Create(nom); // appel de la fct de la mère
end;

//Function play
function Human.play() : Boolean;
var
	i, j : integer;
begin
		i := 0;
		j := 0;
		//Si il valide
		if (UMain.ma.getValid()) then begin
				i := sizePlayedPion();
				if (i > 0) then begin
					j := sizePlayedPos();
					if (j > 0) then 
						setScore(getScore() + calcPoints(getPlayedPos(), i))
					else
						reputinSac(getPlayedPion());
					clearPosed();
					pioche(i);
					UMain.ma.setSelectPion(Nil);
					exit(True);
				end;
		end;
		//Si il a sélectionné un pion
		if (UMain.ma.getSelectPion() <> Nil) THEN begin
			//Si il a sélectionné une position
			if (UMain.ma.getSelectPos() <> Nil) THEN begin
				i := sizePlayedPion();
				j := sizePlayedPos();
				//Si il pas mis de pion dans la pioche
				if ((j > 0) or (i = 0)) then begin
					for i := 0 to 5 do begin
						if (getPlayedPos()[i] = Nil) then begin
								new(self.getPlayedPos()[i]);
								new(self.getPlayedPion()[i]);
								self.getPlayedPos()[i]^ := UMain.ma.getSelectPos()^;
								self.getPlayedPion()[i]^ := UMain.ma.getSelectPion()^;
								break;
						end;
					end;
					//Vérifie si il peut le mettre
					if (verifPlace(UMain.ma.getSelectPion()^, UMain.ma.getSelectPos()^, getPlayedPos())) then begin
						poser(UMain.ma.getSelectPion()^, UMain.ma.getSelectPos()^.x, UMain.ma.getSelectPos()^.y);
						UMain.ma.setSelectPion(Nil);
					end else begin
						dispose(self.getPlayedPos()[i]);
						dispose(self.getPlayedPion()[i]);
						self.getPlayedPos()[i] := Nil;
						self.getPlayedPion()[i] := Nil;
					end;
				end;
			//Vérifie si il a pas déjà placé un pion
			end else if (UMain.ma.getPioche()) then begin
				i := sizePlayedPos();
				if (i = 0) then begin
					for i := 0 to 5 do begin
						if (getPlayedPion()[i] = Nil) then begin
							new(self.getPlayedPion()[i]);
							self.getPlayedPion()[i]^ := UMain.ma.getSelectPion()^;
							supprHand(getPlayedPion()[i]^);
							UMain.ma.setSelectPion(Nil);
							break;
						end;
					end;
				end;
			end;
		end;
	exit(False);
end;

begin
end.
