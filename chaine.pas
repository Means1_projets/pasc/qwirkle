unit chaine;



interface

	uses Utils;
	function longueur(tete : bout_sac) : Integer;
	function creerNoeud(elt : Pion) : bout_sac;
	function creerNoeudD(elt : Pion; tete : bout_sac) : bout_sac;
	function supprimerNoeudD(tete : bout_sac) : bout_sac;
	procedure creerNoeudF(elt : Pion; tete : bout_sac);
	procedure supprimerNoeudF(tete : bout_sac);
	procedure creerNoeudM(elt : Pion; Pos : Integer; tete : bout_sac);
	procedure supprimerNoeudM(Pos : Integer; tete : bout_sac);
	function rechercherV(tete : bout_sac; k: Pion): Integer;
	function supprimerVoulue(elt : Pion; tete : bout_sac): bout_sac;
	function supprimerToutVoulue(elt: Pion; tete: bout_sac): bout_sac;
	function rechercherK(teteListe : bout_sac; k: Integer): PPion;
	function supprimerDoublons(tete : bout_sac): bout_sac;
	procedure delete(tete : bout_sac);

implementation

	procedure delete(tete : bout_sac);
	begin
		if (tete^.suivant <> Nil) then begin
			delete(tete^.suivant);
			dispose(tete^.suivant);
		end;
	end;
	function longueur(tete : bout_sac) : Integer;
	var n : Integer; tmp : bout_sac; 

	begin
		n := 0;
		tmp := tete;
			while(tmp <> Nil) do begin
				n := n+1;
				tmp := tmp^.suivant;
			end;
		longueur := n;
	end;

	function creerNoeud(elt : Pion) : bout_sac;
	var nv : bout_sac;
	begin
		new(nv);
		nv^.valeur := elt;
		nv^.suivant := Nil;
		creerNoeud := nv;
	end;

	function creerNoeudD(elt : Pion; tete : bout_sac) : bout_sac;
	var nv : bout_sac;
	begin
		new(nv);
		nv^.valeur := elt;
		nv^.suivant := tete;
		creerNoeudD := nv;
	end;

	function supprimerNoeudD(tete : bout_sac) : bout_sac;
	begin
		supprimerNoeudD := tete^.suivant;
		disPose(tete);
	end;

	procedure creerNoeudF(elt : Pion; tete : bout_sac);
	var nv, l : bout_sac;
	begin
		l := tete;
		while l^.suivant <> nil do
			l := l^.suivant;
		new(nv);
		nv^.valeur := elt;
		nv^.suivant := Nil;
		l^.suivant := nv;
	end;

	procedure supprimerNoeudF(tete : bout_sac);
	var l : bout_sac; i, lng : Integer;
	begin
		l := tete;
		lng := longueur(tete)-2;
		for i := 1 to lng do
			l := l^.suivant;
		disPose(l^.suivant);
		l^.suivant := nil;
	end;


	procedure creerNoeudM(elt : Pion; Pos : Integer; tete : bout_sac);
	var l, l2, nv : bout_sac; i : Integer;
	begin
		l := tete;
		l2 := tete;
		for i:= 1 to Pos-2 do
				l := l^.suivant;
		for i := 1 to Pos-1 do
			l2 := l2^.suivant;
		new(nv);
		nv^.valeur := elt;
		nv^.suivant := l2;
		l^.suivant := nv;
	end;

	procedure supprimerNoeudM(Pos : Integer; tete : bout_sac);
	var l, l2 : bout_sac; i : Integer;
	begin
		l := tete;
		l2 := tete;
		for i := 1 to Pos-2 do
			l := l^.suivant;
		for i := 1 to Pos do
			l2 := l2^.suivant;
		disPose(l^.suivant);
		l^.suivant := l2;
	end;

	function rechercherK(teteListe : bout_sac; k: Integer): PPion;
	var i : Integer; l : bout_sac;
	var p : PPion;
	begin
		l := teteListe;
		i := 1;
		p := Nil;
		while (i < k) and (l <> Nil) do begin
			l := l^.suivant;
			i := i + 1;
		end;
		if (l <> Nil) then begin
			new(p);
			p^ := l^.valeur;
		end;
		exit(p);
	end;

	function rechercherV(tete : bout_sac; k: Pion): Integer;
	var i, base : Integer; l : bout_sac;
	begin
		base := -1;
		l := tete;
		i := 1;
		while l <> nil do begin
			if l^.valeur = k then
				exit(i);
			i := i + 1;
			l := l^.suivant;
		end;
		rechercherV := base;
	end;

	function supprimerVoulue(elt : Pion; tete : bout_sac): bout_sac;
	var Pos : Integer;
	begin
		Pos := rechercherV(tete, elt);
		if Pos = 1 then
			tete := supprimerNoeudD(tete)
		else if Pos = longueur(tete) then
			supprimerNoeudF(tete)
		else if Pos <> -1 then
			supprimerNoeudM(Pos, tete);
		supprimerVoulue := tete;
	end;

	function supprimerToutVoulue(elt: Pion; tete: bout_sac): bout_sac;
	begin
	repeat
		tete := supprimerVoulue(elt, tete);
	until rechercherV(tete,elt) = -1;
	supprimerToutVoulue := tete;
	end;

	function supprimerDoublons(tete : bout_sac): bout_sac;
	var mem : Pion; l2 : bout_sac;
	begin
		l2 := nil;
		mem := tete^.valeur;
		l2 := creerNoeud(mem);
		tete := supprimerToutVoulue(mem, tete);
		while tete <> nil do begin
			mem := tete^.valeur;
			creerNoeudF(mem,l2);
			tete := supprimerToutVoulue(mem, tete);
		end;
		supprimerDoublons := l2;
	end;

	
end.
